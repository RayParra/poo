﻿using System;

namespace herencia_cascada
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Base claseBase = new Base();
            Console.WriteLine(claseBase.quienSoy());
            //La salida será
            //Soy Base
            A claseA = new A("A");
            Console.WriteLine(claseA.quienSoy());
            //Salida:
            //Me llamo A
        }
    }

    public class Base{
        protected string Nombre;

         public Base(){
            Nombre = "Base";
        }
        //Regresa un string
        public virtual string quienSoy(){
            return "Soy " + Nombre;
        }
    }

    //Clase que sobreescribe el metodo quienSoy
    public class A : Base
    {
        public A(string nombre)
        {
            Nombre = nombre;
        }

        public override string quienSoy()
        {
            return "Me llamo " + Nombre;
        }
    }

}

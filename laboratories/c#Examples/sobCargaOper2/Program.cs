﻿using System;

namespace sobCargaOper2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var q = new Empleado(2000);
            var p = new Empleado(1000);
            var z = new Supervisor("Sistemas");
            Console.WriteLine(p.tax(p));
            Console.WriteLine(p + q);// sobre carga de operadores
            Console.WriteLine(p.tax(p) + q.tax(q)); // que sucede aqui???
            z.set_name("Foo Bar");
            z.set_salario(1500);
            Console.WriteLine(z.get_name() +  z.get_puesto() + z.get_salario());
        }
    }

    public class Persona{
        public string name;

        public void set_name(string newname){
            name = newname;
        }

        public string get_name(){
            return name;
        }
    }

    public class Empleado : Persona{
        public Empleado(){

        }
        
        public Empleado(int newsalario){
            salario = newsalario;
        }

        public Empleado(int newsalario, string newdepto){
            salario = newsalario;
            depto = newdepto;
        }


        public int salario;
        public string depto;

        public int get_salario(){
            return salario;
        }

        public void set_salario(int newsalario){
            salario = newsalario;
        }

        public double tax(Empleado p){
            var taxes = 0.0;
                taxes = p.salario * 0.11;
            return taxes;
        }

        public static int operator +(Empleado j, Empleado k){
            return j.salario + k.salario;
        }

    }

    public class Supervisor : Empleado{
        public Supervisor(){

        }

        public Supervisor(string newpuesto){
            puesto = newpuesto;   
        }

        public string puesto;

        public string get_puesto(){
            return puesto;
        }
    }
    
}


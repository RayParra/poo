﻿using System;

namespace clases_abstractas2
{
    public abstract class Base{
        public int id {get; private set;}
        public string name {get; private set;}
        public string description {get; private set;}
        public string user {get; set;}

        public abstract bool is_active();
    }

    public class Category : Base{

        public int code {get; private set;}

        public override  bool is_active(){
            return true;
        }
    }

    public class Subcategory : Category{
        public string category {get; private set;}

        public override bool is_active(){
            return false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var p = new Subcategory();
            Console.WriteLine($"La categoria tiene estatus Activo? {p.is_active()}");
            p.user = "FOO BAR";
            Console.WriteLine($"El usuario asociado es: {p.user}");
        }
    }
}

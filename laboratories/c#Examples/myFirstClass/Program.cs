﻿using System;

namespace myFirstClass
{
    class MyOwnFirstClass
    {
        // atributos
        public static string name = "Ray";

        // metodo principal
        static void Main(string[] args)
        {
            int age = 23;
            Console.WriteLine("Hello World!");
           
            Console.WriteLine(name + age);
            set_name("Foo Bar");
            Console.WriteLine(name);
        }

        // my own methods
        public static void set_name(string newname){
            name = newname;
        }
    }
}

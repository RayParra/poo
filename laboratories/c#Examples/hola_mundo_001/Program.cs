﻿using System;

namespace hola_mundo_001
{
    class Program
    {

        // atributos de la clase Program
        static string class_name = "Program";

        // metodos de la clase Program

        static void Main(string[] args)
        {
            
            // Persona p = new Persona()
            var p = new Persona();// instancia de la clase Persona
            
            Console.WriteLine("Hello World!");
            Console.WriteLine(p.get_age());
           //Console.WriteLine(p.age);
            Console.WriteLine(p.set_age(34));
            Console.WriteLine(p.get_age());
            Console.WriteLine(p.attr);

            // atributos locales
           // Console.WriteLine(class_name);

            // metodos locales
            Console.WriteLine(get_class_name());

        }

     public static string get_class_name(){
            return class_name;
        }
       

     class Persona{

            // atributos
            int age = 23;
            string name = "Ray";
            public int attr = 10; 

            // Metodos
            public int get_age(){
                return age;
            }
            public int set_age(int newage){
                age = newage;
                return 0;
            }
        }
       

    }
}

﻿using System;

namespace sobre_carga_operator
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = 30;
            var y = 90;
           // var p = new Person(9);
           // var q = new Person(6);
           // Console.WriteLine(p + q);
            Console.WriteLine("Hello World!");
            var foo = new Employ(15000);
            var bar = new Employ(20000);
            Console.WriteLine(foo + bar);
            Console.WriteLine(10 + 15);
            Console.WriteLine(x + y);
        }
    }

    public class Employ{
        public Employ(int newsalary){
            salary = newsalary;
        }

        public int salary;

        public static int operator +(Employ X, Employ Y){
            return X.salary + Y.salary; 
        }

    }
    public class Person{
        public Person(int salary){
            person_salary = salary;
        }

        public int person_salary;


    }
}

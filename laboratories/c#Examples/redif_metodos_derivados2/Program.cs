﻿using System;

namespace redif_metodos_derivados2
{
    abstract class Base{
        public abstract int get_area();
    }

    class Program{

        public class Square : Base{
            int side;
            
            public Square(int s) => side = s;

            public override int get_area() => side * side;

            
        }
        static void Main(string[] args)
        {
            var p = new Square(3);
            Console.WriteLine($"El area del cuadrado es: {p.get_area()}");
        }
    }
}

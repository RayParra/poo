﻿using System;

namespace override
{
    class Program
    {
        static void Main(string[] args)
        {
            var base = new Base("Foo");
            Console.WriteLine(base.whoIam());
        }
    }


    public class Base{
        protected string name;
        
        public Base(string newname){
            name = newname;
        }

        public virtual string whoIam(){
            return "I am: " + name;
        }

    }

    public class ChildA : Base{
        // aqui queremos aplicar el override
    }

    public class ChildB : Base{
        // aqui tambien queremos aplicar el override

    }


}

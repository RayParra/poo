﻿using System;

namespace herencia_clases
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var p = new Cuenta();

            p.name = "Foo Bar";
            Console.WriteLine(p.name);
        }

    }

    public class Cliente{
        public string name;
        public string city;
    }

    public class Cuenta : Cliente {
        public string tipo_cuenta;
        public string sucursal;
    }
}

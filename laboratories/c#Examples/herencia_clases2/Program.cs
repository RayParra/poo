﻿using System;

namespace herencia_clases2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var p = new Auto();
            var q = new Avion();
            var z = new Transporte();

            p.tipo = "Terrestre";
            p.model = "Coupe";
            p.avanzar();

            Console.WriteLine(p.tipo + " " + p.model);
            q.avanzar();
            Console.WriteLine("----------------------");
           // z.activar();// Que pasa si ejecutamos esta linea??
            // p.activar();// Que pasa si ejecutamos esta linea?? 
            
            p.iniciar();

            q.get_horas_vuelo();
            q.set_horas_vuelo(300);
            q.get_horas_vuelo();

            q.info_pilot();
        }
    }

    public class Transporte{
        public string tipo;
        public int capacidad_pasajeros;
        protected int horas_activo;

        public void avanzar(){
            Console.WriteLine("Avanzando ..");
        }

        protected void activar(){
            Console.WriteLine("Activado..");
        }
    }

    public class Auto : Transporte{
        public string model;


        public void iniciar(){
            Console.WriteLine("Iniciiando...");
            activar();
        }
    }

    public class Avion : Transporte{
        public string model_type;
        protected int num_pilot = 90;

        public void set_horas_vuelo(int horas){
            horas_activo = horas;
        }

        public void get_horas_vuelo(){
            Console.WriteLine(horas_activo);
        }

        public void despegar(){
            Console.WriteLine("despegando...");
            activar();
        }

        public void info_pilot(){
            Console.WriteLine(num_pilot);
        }
    }

}

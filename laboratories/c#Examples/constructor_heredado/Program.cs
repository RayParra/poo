﻿using System;

namespace constructor_heredado
{
    public class Person{
        public Person(string nombre, string apellido){
            name = nombre;
            last_name = apellido;
        }
        public string name;
        public string last_name;

        public string get_info(){
            return name + " " + last_name;
        }
    }

    public class Professor : Person{
        public Professor(string nombre, string apellido, string email):base(nombre, apellido){
            mail = email;
        }

        public string mail;
       
    }

    

    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person("Foo", "Bar");
            Person q = new Professor("Jhon", "Wick",  "q@q.com");
            Console.WriteLine(q.get_info());
        }
    }
}

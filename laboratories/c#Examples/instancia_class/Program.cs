﻿using System;

namespace instancia_class
{
    class Servidor{
        // public Servidor(){}
        private string server_name;
        public Servidor(string name){
            server_name = name;
        }
       
        public string get_server_name(){
            return server_name;
        }

        public void set_server_name(string newname){
            server_name = newname;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Servidor p = new Servidor("WEBSERVER");
            var q = new Servidor("DBMS");
            q.set_server_name("POSTGRESQL");
            Console.WriteLine("Hello World!");
            Console.WriteLine(p.get_server_name());
            Console.WriteLine(q.get_server_name());
        }
    }
}

﻿using System;

namespace test03
{
    class MyClass
    {
        // Mis atributos
        public static string name = "Ray";
        static void Main(string[] args)
        {
            // mis variables
            int age = 23;

            // Instancias

            PorCien p = new PorCien();

            Console.WriteLine("Hello World!");
            Console.WriteLine("Hello World!" + name);
            Console.WriteLine("Hello World!" + age);
            set_name("Foo Bar");
            Console.WriteLine("Hello World!" + name);

            Console.WriteLine(p.porciento(0.85) + "%");

        }
        public static void set_name(string newname){
            name = newname;
        }
    public class PorCien{
        public int valpor = 100;

        public double porciento(double cantidad){
            return cantidad * valpor;
        }
    }
    }
}

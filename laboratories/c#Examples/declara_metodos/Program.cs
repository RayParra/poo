﻿using System;

namespace declara_metodos
{
    class Program
    {
        // atributo
        public static string message;

        static void Main(string[] args)
        {
            string msg = "Hola Mundo C# soy el mensaje de prueba";

            Console.WriteLine("Escribe un mensaje:  ");
            msg = Console.ReadLine();
            set_message(msg);
            Console.WriteLine(get_message());
        }

    public static string get_message(){
        return message;
    }    

    public static void set_message(string newmsg){
        message = newmsg;
    }    

    }
}

﻿using System;
using System.IO;

namespace flujo_archivos
{
    class Program
    {
        private const string FILE_NAME = "test01.txt";

        static void Main(string[] args)
        {
            if (!File.Exists(FILE_NAME))
            {
                Console.WriteLine($"{FILE_NAME} does not Exist!");
                return;
            }

            FileStream myfile = new FileStream(FILE_NAME, FileMode.Open, FileAccess.Read, FileShare.Read);

            using (StreamReader sr = new StreamReader(myfile))
            {
                string input;
                while (sr.Peek() > -1)
                {
                     input = sr.ReadLine();
                     Console.WriteLine(input);
                }
                 
            }
            //Console.WriteLine("Hello World!");
        }
    }
}

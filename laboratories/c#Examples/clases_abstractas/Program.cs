﻿using System;

namespace clases_abstractas
{

    public abstract class BaseTriangle{
        
        public abstract string perimetro();

        public double calcular_area(int baset, int altura){
            double area = (baset * altura) / 2;
            return area;
        }
    }

    public class Triangle_Equilatero : BaseTriangle{
        
        public override string perimetro(){
            //throw new NotImplementedException();
            return "Implementing a Abstract Method.";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var t = new Triangle_Equilatero();
            Console.WriteLine(t.perimetro());
            Console.WriteLine(t.calcular_area(34, 78));
        }
    }
}

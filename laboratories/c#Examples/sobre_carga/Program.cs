﻿using System;

namespace sobre_carga
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("la suma es: " + suma(3, 9, 6));
            Console.WriteLine("la suma es: " + suma(3, 9));
            Console.WriteLine("la suma es: " + suma("7", 4));
        }

        public static int suma(int a, int b){
            return a + b;
        }

        public static int suma(int a, int b, int c){
            return a + b + c;
        }

        public static string suma(string a, int b){
            return a;
        }
    }
}

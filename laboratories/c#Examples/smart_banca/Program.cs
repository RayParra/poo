﻿using System;

namespace smart_banca
{
    class Program
    {
        static void Main(string[] args)
        {
            Client bar = new Client();// constructor por default
            var jw = new Client("Jhon Wick"); // constructor sobre cargado con un parametro
            var foo = new Client("Foo Bar", "TJ"); //// constructor sobre cargado con dos parametros
            foo.get_client_name();
            foo.set_client_city("Tijuana");
            foo.get_client_city();
            /* 
            Console.WriteLine(Escribe un nombre:  );
            newname = Console.ReadLine();
            foo.set_client_name(newname);
            */
            foo.set_client_name("Jhon Wick");
            foo.get_client_name();
            // client_name = "Ray"//Error, no podemos acceder a miembros private
           // foo.client_name = "Ray";// ¿que sucede aqui?
        }
        
    }

    class Client{
        // Constructores
        public Client(){

        }
        
        public Client(string name){
            client_name = name;
        }

        public Client(string name, string ciudad){
            client_name = name;
            client_ciudad = ciudad;
        }

        // Atributos
        private string client_name;
        public string client_ciudad;

        //Metodos
        public void get_client_name(){
            Console.WriteLine(client_name);
        }

        public void get_client_city(){
            Console.WriteLine(client_ciudad);
        }

        public void set_client_city(string newcity){
            client_ciudad = newcity;
        }

        public void set_client_name(string newname){
            client_name = newname;
        }
    }

    class Banca{
    }
}

﻿using System;

namespace try_catch
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Escribe un valor: ");
                string line = Console.ReadLine();
                var x = Int32.Parse(line);
                var sqrt = x * x;
                Console.WriteLine($"El cuadrado de: {x} es {sqrt} ");  
                Console.WriteLine("El codigo del Enigma es... : XD");
            }
            catch (FormatException)
            {
                
                Console.WriteLine($"Los valores deben ser unicamente del tipo numerico. ..");
            }
           Console.WriteLine("Aqui continua el Programa..");
        }
    }
}

﻿using System;

namespace polimorfismo
{

    public class Shape{
        public int X {get; private set;}
        public int Y {get; private set;}
        public int Height {get; private set;}
        public int Width {get; private set;}

        public virtual void Draw(){
            Console.WriteLine("Performing base class drawing task");
        }
    }

    public class Triangle : Shape{
        public override void Draw(){
            Console.WriteLine("Drawing a Triangle");
            base.Draw();
        }
    }

    public class Circle : Shape{
        public override void Draw(){
            Console.WriteLine("Drawing a Circle");
            base.Draw();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            var p = new Triangle();
            var q = new Circle();
            p.Draw();
            q.Draw();
        }
    }
}

﻿using System;

namespace herencia_sobre_escritura
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Base cbase = new Base();
            DerivadaA da = new DerivadaA("Derivada Foo Bar");
            Console.WriteLine(cbase.whoIam());
            Console.WriteLine(da.whoIam());
            Console.WriteLine(da.noescritura());
        }
    }

    public class Base{
        protected string Nombre;

        public Base(){
            Nombre = "Foo Bar";
        }

        public virtual string whoIam(){
            return "I am: " + Nombre;
        }

        public string noescritura(){
            return "No sobre escritura";
        }
    }

    public class DerivadaA : Base{
        public DerivadaA(string nombre){
            Nombre = nombre;
        }

        public override string whoIam(){
            return "I am DA: " + Nombre;
        }

        // public override string noescritura(){
        //     return "Si sobre escritura";
        // }
        // public new string whoIam()
    }
}

﻿using System;

namespace varpolimorfica
{

    public class Animal {
        public string name {get; set;}
        
        public Animal(){

        }

        public Animal(string _name){
            name = _name;
        }

        public virtual void mover(){
            Console.WriteLine("Im Moving ... ");
        }


    }

    public class Perro : Animal{
        public override void  mover(){
            Console.WriteLine("My Own Dog Moving ...");
        }
    }

    public class Gato : Animal{
        public override void  mover(){
            Console.WriteLine("My Own Cat Moving ...");
        }
    }

    public class Pez : Animal{
        public override void mover(){
            Console.WriteLine("Moving in the Wather .. ");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Animal pet = new Animal("Foo");
            Console.WriteLine("This is my Mover Method ...");
            pet.mover();

            pet = new Gato();
            pet.mover();
            //var cat = new Gato();
            //cat.mover();

            pet = new Pez();
            pet.mover();
        }
    }
}

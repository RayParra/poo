#include <iostream>
#include <string>

using namespace std;

class holaMundo{
    public:
    // atributo
    string message;
    // metodos
    string get_message();
    void set_message(string msg);
    private:
    protected:
};

string holaMundo::get_message(){
    return message;
}

void holaMundo::set_message(string msg){
    message = msg;
}

int main(){
    holaMundo p;
    string msg;

    cout << "Escribe un mensaje:  " << endl;
    getline(cin, msg);
    p.set_message(msg);
    cout << "El mensaje dice: " << p.get_message() << endl;

    return 0;
}
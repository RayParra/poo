#include <iostream>
#include <string>

using namespace std;

class Employ{
    public:
    // atributos
    string employ_name;
    int employ_salary;

    // metodos
    string get_name();
    int get_salary();
    void set_name(string newname);
    void set_salary(int newsalary);

    private:
    protected:
};

string Employ::get_name(){
    return employ_name;
}

void Employ::set_name(string newname){
    employ_name = newname;
}

int Employ::get_salary(){
    return employ_salary;
}

void Employ::set_salary(int newsalary){
    employ_salary = newsalary;
}


int main(){
    Employ p;
    string name;
    int salary;
    
    cout << "Escribe el nombre del emplado:  " << endl;
    getline(cin, name);
    p.set_name(name);
    cout << "El nombre del empleado es:  " << p.get_name() << endl;

    cout << "Escribe el salario del emplado:  " << endl;
    cin >> salary;
    p.set_salary(salary);
    cout << "El salario del empleado es:  " << p.get_salary() << endl;

    return 0;
}
#include <iostream>
#include <string.h>

using namespace std;

class empleado
{
   
    public:
     // atributos
    

    // metodos
    string get_name();
    int get_salary();
    void set_name(string newname);
    void set_salary(int newsalary);

    private:
    // atributos
    string name;
    int salario;

    protected:

};

string empleado::get_name(){
    return name;
}

void empleado::set_name(string newname){
    name = newname;
}
int main(){
    empleado p;
    string employ_name;
    int employ_salary;

    cout << "Escribe el nombre del Empleado:  " << endl;
    getline(cin, employ_name);
    cout << "Escribe el salario del empleado:  " << endl;
    cin >> employ_salary;
    //cout << p.get_name() << endl;
    p.set_name(employ_name);
    cout << "El nuevo nombre es: " << p.get_name() << endl;
    return 0;
}

class HolaMundo:
    def __init__(self):
        self.message = "Default"
        
    def get_message(self):
        return self.message
    
    def set_message(self, msg):
        self.message = msg
        return True


p = HolaMundo()

print(p.get_message())
print(p.set_message("Hola Mundo"))
print(p.get_message())